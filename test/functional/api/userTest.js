const chai = require('chai')
const expect = chai.expect
const request = require('supertest')
const MongoMemoryServer = require('mongodb-memory-server').MongoMemoryServer
const User = require('../../../models/User')
const mongoose = require('mongoose')

const _ = require('lodash')
let server
let mongod
let db, validID
const invalidID = '5db5c9288ad68f0b2cccc021'

describe('Userss', () => {
    var token
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: './test/database',
                    dbName: process.env.MONGO_DB, // by default generate random dbName
                }
            })
            // Async Trick - this ensures the database is created before
            // we try to connect to it or start the server
            mongoose.connect(process.env.MONGO_URI+process.env.MONGO_DB, {
                useCreateIndex: true,
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            server = require('../../../bin/www')
            db = mongoose.connection
        } catch (error) {
            console.log(error)
        }
    })

    after(async () => {
        try {
            await db.dropDatabase()
        } catch (error) {
            console.log(error)
        }
    })

    beforeEach(async () => {
        try {
            await User.deleteMany({})
            let user = new User()
            user.username = 'test1'
            user.email = 'test1@test.com'
            user.password = 'test123456'
            user.createdTime = new Date
            await user.save()
            user = new User()
            user.username = 'test2'
            user.email = 'test2@test.com'
            user.password = 'test123456'
            user.createdTime = new Date
            await user.save()
            user = await User.findOne({username: 'test1'})
            validID = user._id
        } catch (error) {
            console.log(error)
        }
    })

    describe('POST /user/register', async () => {
        const user3 = new User({
            username: 'test3',
            email: 'test3@test.com',
            password: 'test123456'
        })
        const user4 = new User({
            username: 'test1',
            email: 'test4@test.com',
            password: 'test123456'
        })
        const user5 = new User({
            username: 'test5',
            email: 'test1@test.com',
            password: 'test123456'
        })
        it('should register successfully and return msg', done => {
            request(server)
                .post('/user/register')
                .send(user3)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(1)
                    expect(res.body.msg).to.equal('Successful to register!')
                    done(err)
                })
        })
        it('should failed to register and return error msg', done => {
            request(server)
                .post('/user/register')
                .send(user4)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(0)
                    expect(res.body.errorMsg).to.equal('Failed to register: username has been used!')
                    done(err)
                })
        })
        it('should failed to register and return error msg', done => {
            request(server)
                .post('/user/register')
                .send(user5)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(0)
                    expect(res.body.errorMsg).to.equal('Failed to register: email has been used!')
                    done(err)
                })
        })
    })

    describe('POST /user/login', () => {
        const user = new User({
            username: 'test1',
            password: 'test123456'
        })
        const incorrectPasswordUser = new User({
            username: 'test1',
            password: 'test12345'
        })
        const invalidUsernameUser = new User({
            username: 'test3',
            password: 'test12345'
        })
        it('should Login in successfully', done => {
            request(server)
                .post('/user/login')
                .send(user)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(1)
                    expect(res.body.token).to.be.not.null
                    token = 'Bearer ' + res.body.token
                    done(err)
                })
        })
        it('should failed to login', done => {
            request(server)
                .post('/user/login')
                .send(incorrectPasswordUser)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('Incorrect Password!')
                    done(err)
                })
        })
        it('should failed to login', done => {
            request(server)
                .post('/user/login')
                .send(invalidUsernameUser)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('No this user')
                    done(err)
                })
        })
    })

    describe('GET /users', () => {
        it('should GET all the users successfully', done => {
            request(server)
                .get('/users')
                .set('Accept', 'application/json')
                // .set('Authorization',token)// add jsonwebtoken
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a('array')
                    expect(res.body.length).to.equal(2)
                    let result = _.map(res.body, user => {
                        return {
                            username: user.username,
                            email: user.email
                        }
                    })
                    expect(result).to.deep.include({
                        username: 'test1',
                        email: 'test1@test.com'
                    })
                    expect(result).to.deep.include({
                        username: 'test2',
                        email: 'test2@test.com'
                    })
                    done(err)
                })
        })
    })

    describe('GET /user/:id', () => {
        it('should return the user information', done => {
            request(server)
                .get(`/user/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body.user).to.deep.include({username: 'test1', email: 'test1@test.com'})
                    done(err)
                })
        })

        it('should failed to return the not-exist user information', done => {
            request(server)
                .get(`/user/${invalidID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('No This ID')
                    done(err)
                })
        })
    })


    describe('DELETE /user/delete/:id', () => {
        it('should failed to delete the not-exist user', done => {
            request(server)
                .delete(`/user/delete/${invalidID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('No Related Data')
                    done(err)
                })
        })
        it('should delete the user', () => {
            request(server)
                .delete(`/user/delete/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(res => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body.msg).to.equal('Successful to delete')
                })
        })
        after(() => {
            return request(server)
                .get(`/user/${validID}`)
                .expect(200)
                .then(res => {
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('No This ID')
                })
        })
    })

    describe('PUT /user/update', () => {
        it('should failed to update a not-exist user', done => {
            const invalidUser = {
                _id: invalidID,
                username: 'updatedUsername1',
            }
            request(server)
                .put('/user/update')
                .send(invalidUser)
                .set('Authorization', token)// add jsonwebtoken
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('No this user')
                    done(err)
                })
        })

        it('should failed to update the user without JWT', () => {
            const updateUser = {
                _id: validID,
                username: 'updatedUsername1',
            }
            request(server)
                .put('/user/update')
                .send(updateUser)
                // .set('Authorization',token)// add jsonwebtoken
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(401)
                .then(res => {
                    expect(res.body.code).to.equal(401)
                    expect(res.body.error).to.equal('Please login in to visit')
                })
        })

        it('should update the user successfully with JWT', () => {
            const updateUser = {
                _id: validID,
                username: 'updatedUsername1',
            }
            request(server)
                .put('/user/update')
                .send(updateUser)
                .set('Authorization', token)// add jsonwebtoken
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(res => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body.msg).to.equal('Successful to update!')
                })
        })
        after(() => {
            return request(server)
                .get(`/user/${validID}`)
                .expect(200)
                .then(res => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body.user).to.deep.include({username: 'updatedUsername1'})
                })
        })
    })

})

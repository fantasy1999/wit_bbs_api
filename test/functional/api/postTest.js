const chai = require('chai')
const expect = chai.expect
const request = require('supertest')
const MongoMemoryServer = require('mongodb-memory-server').MongoMemoryServer
const Post = require('../../../models/Post')
const User = require('../../../models/User')
const mongoose = require('mongoose')

const _ = require('lodash')
let server
let mongod
let db, validID
const invalicID = '5db5c9288ad68f0b2cccc021'

describe('Posts', async () => {
    var token
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: './test/database',
                    dbName: process.env.MONGO_DB,// by default generate random dbName
                }
            })
            // Async Trick - this ensures the database is created before
            // we try to connect to it or start the server
            mongoose.connect(process.env.MONGO_URI+process.env.MONGO_DB, {
                useFindAndModify: false,
                useCreateIndex: true,
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            server = require('../../../bin/www')
            db = mongoose.connection
        } catch (error) {
            console.log(error)
        }
    })

    after(async () => {
        try {
            await db.dropDatabase()
        } catch (error) {
            console.log(error)
        }
    })

    beforeEach(async () => {
        try {
            await Post.deleteMany({})
            await User.deleteMany({})
            let user = new User({
                username: 'testJWT',
                password: 'test',
                email: 'test@test.com',
                createdTime: new Date
            })
            await user.save()
            user = await User.findOne({username: 'testJWT'})
            let post = new Post()
            post.title = 'title1'
            post.content = 'This is content1'
            post.creatorId = user._id,
            post.createdTime = new Date
            post.updatedTime = new Date
            await post.save()
            post = new Post()
            post.title = 'title2'
            post.content = 'This is content2'
            post.creatorId = user._id
            post.createdTime = new Date
            post.updatedTime = new Date
            await post.save()
            post = await Post.findOne({title: 'title1'})
            validID = post._id
        } catch (error) {
            console.log(error)
        }
    })

    describe('POST /user/login', () => {
        it('should login to get token before test', done => {
            const user = new User({
                username: 'testJWT',
                password: 'test'
            })
            request(server)
                .post('/user/login')
                .send(user)
                .expect(200)
                .end((err, res) => {
                    token = 'Bearer ' + res.body.token
                    done(err)
                })
        })
    })

    describe('POST /post', () => {
        it('should post successfullya with JWT', done => {
            const post = new Post({
                title: 'title3',
                content: 'This is content3',
                creatorId: '5db4b196869302154c6f0e71',
                createdTime: new Date,
                updatedTime: new Date
            })
            request(server)
                .post('/post')
                .send(post)
                .set('Authorization',token)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(1)
                    done(err)
                })
        })
        it('should failed to post without JWT', done => {
            const post = new Post({
                title: 'title3',
                content: 'This is content3',
                creatorId: '5db4b196869302154c6f0e71',
                createdTime: new Date,
                updatedTime: new Date
            })
            request(server)
                .post('/post')
                .send(post)
                .expect(401)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(401)
                    expect(res.body.error).to.equal('Please login in to visit')
                    done(err)
                })
        })
    })

    describe('GET /post/:id', () => {
        it('should return the post detail', done => {
            request(server)
                .get(`/post/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.post).to.deep.include({title: 'title1', content: 'This is content1'})
                    done(err)
                })
        })
        it('should failed to return a not-exist post detail', done => {
            request(server)
                .get(`/post/${invalicID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('No This Post')
                    done(err)
                })
        })
    })

    describe('GET /all', () => {
        it('should return all the posts', done => {
            request(server)
                .get('/all')
                .set('Accept', 'application/json')
                .expect(200)
                .end((err, res) => {
                    expect(res.body.posts).to.be.a('array')
                    expect(res.body.posts.length).to.equal(2)
                    done(err)
                })
        })
    })

    describe('GET /search', () => {
        it('should return all the related posts', done => {
            request(server)
                .get('/search/1')
                .set('Accept', 'application/json')
                .expect(200)
                .end((err, res) => {
                    expect(res.body.posts).to.be.a('array')
                    expect(res.body.posts.length).to.equal(1)
                    expect(res.body.posts[0]).to.deep.include({title: 'title1', content: 'This is content1'})
                    done(err)
                })
        })
    })

    describe('DELETE /post/delete/:id', () => {
        it('should failed to delete a not-exist post', done => {
            request(server)
                .delete(`/post/delete/${invalicID}`)
                .set('Authorization',token)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('No Related Data')
                    done(err)
                })
        })
        it('should failed to delete the post without JWT', done => {
            request(server)
                .delete(`/post/delete/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(401)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(401)
                    expect(res.body.error).to.equal('Please login in to visit')
                    done(err)
                })
        })
        it('should delete the post successfully with JWT', () => {
            request(server)
                .delete(`/post/delete/${validID}`)
                .set('Authorization',token)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then( res => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body.msg).to.equal('Successful to delete')
                })
        })
        after(() => {
            request(server)
                .get(`/post/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then( res => {
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('No This Post')
                })
        })
    })

    describe('PUT /post/update', () => {
        it('should update the post successfully with JWT', done => {
            const body = {
                id: validID,
                title: 'updatedTitle1',
                content: 'This is updatedContent1'
            }
            request(server)
                .put('/post/update')
                .send(body)
                .set('Authorization',token)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body.post).to.deep.include({title: 'updatedTitle1',content: 'This is updatedContent1'})
                    done(err)
                })
        })
        it('should failed to update the post without JWT', done => {
            const body = {
                id: validID,
                title: 'updatedTitle1',
                content: 'This is updatedContent1'
            }
            request(server)
                .put('/post/update')
                .send(body)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(401)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(401)
                    expect(res.body.error).to.equal('Please login in to visit')
                    done(err)
                })
        })
        it('should failed to update a not-exist post', done => {
            const body = {
                id: invalicID,
                title: 'updatedTitle1',
                content: 'This is updatedContent1'
            }
            request(server)
                .put('/post/update')
                .send(body)
                .set('Authorization',token)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('No this post')
                    done(err)
                })
        })
    })
})

const chai = require('chai')
const expect = chai.expect
const request = require('supertest')
const MongoMemoryServer = require('mongodb-memory-server').MongoMemoryServer
const Comment = require('../../../models/Comment')
const User = require('../../../models/User')
const mongoose = require('mongoose')

const _ = require('lodash')
let server
let mongod
let db, validID
const invalidID = '5db5c9288ad68f0b2cccc02b'

describe('Comments', () => {
    var token
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: './test/database',
                    dbName: process.env.MONGO_DB, // by default generate random dbName
                }
            })
            // Async Trick - this ensures the database is created before
            // we try to connect to it or start the server
            mongoose.connect(process.env.MONGO_URI+''+process.env.MONGO_DB, {
                useCreateIndex: true,
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            server = require('../../../bin/www')
            db = mongoose.connection
        } catch (error) {
            console.log(error)
        }
    })

    after(async () => {
        try {
            await db.dropDatabase()
        } catch (error) {
            console.log(error)
        }
    })

    beforeEach(async () => {
        try {
            await Comment.deleteMany({})
            await User.deleteMany({})
            let user = new User({
                username: 'testJWT',
                password: 'test',
                email: 'test@test.com',
                createdTime: new Date
            })
            await user.save()
            let comment = new Comment()
            comment.parentId = '5db4b196869302154c6f0e71'
            comment.content = 'This is comment1'
            comment.creatorId = '5db4b196869302154c6f0e71'
            comment.createdTime = new Date
            await comment.save()
            comment = new Comment()
            comment.parentId = '5db4b196869302154c6f0e71'
            comment.content = 'This is comment2'
            comment.creatorId = '5db4b196869302154c6f0e71'
            comment.createdTime = new Date
            await comment.save()
            comment = await Comment.findOne({content: 'This is comment1'})
            validID = comment._id
        } catch (error) {
            console.log(error)
        }
    })

    describe('POST /user/login',() => {
        it('should login to get token before test', done => {
            const user = new User({
                username: 'testJWT',
                password: 'test'
            })
            request(server)
                .post('/user/login')
                .send(user)
                .expect(200)
                .end((err, res) => {
                    token = 'Bearer ' + res.body.token
                    done(err)
                })
        })
    })

    describe('POST /comment', () => {
        it('should comment successfully with JWT', done => {
            const comment = new Comment({
                parentId: '5db4b196869302154c6f0e71',
                content: 'This is comment3',
                creatorId: '5db4b196869302154c6f0e71',
                createdTime: new Date
            })
            request(server)
                .post('/comment')
                .send(comment)
                .set('Authorization',token)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(1)
                    expect(res.body.msg).to.equal('Successful to comment')
                    done(err)
                })
        })
        it('should failed to comment without JWT', done => {
            const comment = new Comment({
                parentId: '5db4b196869302154c6f0e71',
                content: 'This is comment3',
                creatorId: '5db4b196869302154c6f0e71',
                createdTime: new Date
            })
            request(server)
                .post('/comment')
                .send(comment)
                .expect(401)
                .end((err, res) => {
                    expect(res.body).to.be.a('object')
                    expect(res.body.code).to.equal(401)
                    expect(res.body.error).to.equal('Please login in to visit')
                    done(err)
                })
        })
    })

    describe('DELETE /comment/delete/:id', () => {
        it('should failed to delete a not-exist post', done => {
            request(server)
                .delete(`/comment/delete/${invalidID}`)
                .set('Authorization',token)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('No Related Data')
                    done(err)
                })
        })

        it('should failed to delete a post without JWT', done => {
            request(server)
                .delete(`/comment/delete/${invalidID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(401)
                .end((err, res) => {
                    expect(res.body.code).to.equal(401)
                    expect(res.body.error).to.equal('Please login in to visit')
                    done(err)
                })
        })

        it('should delete the post', () => {
            request(server)
                .delete(`/comment/delete/${validID}`)
                .set('Authorization',token)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(res => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body.msg).to.equal('Successful to delete')
                })
        })
        after(() => {
            request(server)
                .get(`/comment/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then( res => {
                    expect(res.body.code).to.equal(0)
                    expect(res.body.msg).to.equal('No Related Data')
                })
        })
    })

    describe('GET /comment/:id', () => {
        it('should return a comment detail', done => {
            request(server)
                .get(`/comment/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err,res) => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body.comment).to.deep.include({content: 'This is comment1'})
                    done(err)
                })
        })
    })
})
